/** undefined_specialization.cpp -- No specialization, but a call made. */

#include "speaker.h"
#include "animal.h"

int main()
{
    AnimalSpeaker<Bee> bee_speaker;
    bee_speaker.make_speak();
}
