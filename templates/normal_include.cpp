/** normal_include.cpp -- Include normally.

    This normally includes speaker.h and calls it on Cow and Lion.
*/

#include "speaker.h"
#include "animal.h"

int main()
{
    AnimalSpeaker<Lion> lion_speaker;
    AnimalSpeaker<Cow> cow_speaker;

    lion_speaker.make_speak();
    cow_speaker.make_speak();
}
