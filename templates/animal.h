/** animal.h -- Some types of animals. */

#ifndef TEMPLATES_ANIMAL_H__
#define TEMPLATES_ANIMAL_H__ 1

class Cow {
public:
    void speak();
};

class Lion {
public:
    void speak();
};

class Bee {
public:
    void buzz();
};

#endif // TEMPLATES_ANIMAL_H__
