/** speaker_fwd.h -- Forward declarations for the speaker class. */

#ifndef TEMPLATES_SPEAKER_FWD_H__
#define TEMPLATES_SPEAKER_FWD_H__ 1

template <typename Animal> class AnimalSpeaker {
public:
    AnimalSpeaker() = default;
    void make_speak();

private:
    Animal m_animal;
};

#endif // TEMPLATES_SPEAKER_FWD_H__
