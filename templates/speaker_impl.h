/** speaker_impl.h -- Implementation for the speaker class. */

#ifndef TEMPLATES_SPEAKER_IMPL_H__
#define TEMPLATES_SPEAKER_IMPL_H__ 1

template <typename Animal> void AnimalSpeaker<Animal>::make_speak()
{
    m_animal.speak();
}

#endif // TEMPLATES_SPEAKER_IMPL_H__
