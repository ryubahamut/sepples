/** unordered_include.cpp -- Include in odd order.

    This is identical to normal_include.cpp, but includes the impl
    header at the end. This is to illustrate that so long as the
    compiler is able to find a template anywhere in the same TU,
    it can specialize the template.

    Feel free to think of what will happen when you flip the order
    of the three headers around, and then try and see if the results
    match.
*/

#include "speaker_fwd.h"
#include "animal.h"

int main()
{
    AnimalSpeaker<Lion> lion_speaker;
    AnimalSpeaker<Cow> cow_speaker;

    lion_speaker.make_speak();
    cow_speaker.make_speak();
}

#include "speaker_impl.h"
