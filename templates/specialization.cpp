/** specialization.cpp -- Normal template specialization. */

#include "speaker.h"
#include "animal.h"

// Specializations must _always_ take place before use.
// Note that this specialization is able to transparently
// use stuff from the base class.
template <> void AnimalSpeaker<Bee>::make_speak()
{
    m_animal.buzz();
}

int main()
{
    AnimalSpeaker<Bee> bee_speaker;
    bee_speaker.make_speak();
}
