/** animal.cpp -- Implementation for some animals. */

#include "animal.h"

#include <iostream>

void Cow::speak()
{
    std::cout << "moo!" << std::endl;
}

void Lion::speak()
{
    std::cout << "roar!" << std::endl;
}

void Bee::buzz()
{
    std::cout << "bzzz..." << std::endl;
}
