/** speaker.h -- Makes animals speak. */

#ifndef TEMPLATES_SPEAKER_H__
#define TEMPLATES_SPEAKER_H__ 1

#include "speaker_fwd.h"
#include "speaker_impl.h"

#endif // TEMPLATES_SPEAKER_H__
